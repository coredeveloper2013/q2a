<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->dateTime('join_date')->nullable();
            $table->string('username');
            $table->string('full_name')->nullable();
            $table->string('email', 191)->unique();
            $table->string('location')->nullable();
            $table->string('personal_website')->nullable();
            $table->string('about')->nullable();
            $table->string('avatar')->nullable();
            $table->string('facebook_profile_link')->nullable();
            $table->string('twitter_profile_link')->nullable();
            $table->string('linkedin_profile_link')->nullable();
            $table->string('youtube_profile_link')->nullable();
            $table->boolean('private_message')->default(false);
            $table->boolean('wall_post')->default(false);
            $table->boolean('mass_mailings')->default(false);
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password');
            $table->enum('status', ['active', 'inactive'])->default('inactive');
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
