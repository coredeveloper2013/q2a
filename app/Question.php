<?php

namespace App;

use Cviebrock\EloquentSluggable\Sluggable;
use Cviebrock\EloquentTaggable\Taggable;
use Illuminate\Database\Eloquent\Model;

class Question extends Model
{

    use Taggable, Sluggable;

    /**
     * Return the sluggable configuration array for this model.
     *
     * @return array
     */
    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'question'
            ]
        ];
    }
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'question', 'question_description', 'optional', 'user_id', 'category_id',
    ];

    /**
     * Get the user that owns the question.
     */
    public function user()
    {
        return $this->belongsTo('App\User');
    }

    /**
     * Get the category that owns the question.
     */
    public function category()
    {
        return $this->belongsTo('App\Category');
    }

    /**
     * Get the comments record associated with the question.
     */
    public function comments()
    {
        return $this->hasMany('App\Comment');
    }

    /**
     * Get the answers record associated with the question.
     */
    public function answers()
    {
        return $this->hasMany('App\Answer');
    }

    /**
     * Get the question_vote record associated with the question.
     */
    public function question_vote()
    {
        return $this->hasMany('App\QuestionVote');
    }
}
