<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class QuestionVote extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'vote_count', 'user_id', 'question_id',
    ];

    /**
     * Get the user that owns the question_vote.
     */
    public function user()
    {
        return $this->belongsTo('App\User');
    }

    /**
     * Get the question that owns the question_vote.
     */
    public function question()
    {
        return $this->belongsTo('App\Question');
    }
}
