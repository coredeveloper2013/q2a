<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    /**
     * Get the questions record associated with the user.
     */
    public function questions()
    {
        return $this->hasMany('App\Question');
    }

    /**
     * Get the comments record associated with the user.
     */
    public function comments()
    {
        return $this->hasMany('App\Comment');
    }

    /**
     * Get the answers record associated with the user.
     */
    public function answers()
    {
        return $this->hasMany('App\Answer');
    }

    /**
     * Get the question_vote record associated with the user.
     */
    public function question_vote()
    {
        return $this->hasMany('App\QuestionVote');
    }
}
