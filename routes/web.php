<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'WebsiteController@index');
Route::post('/user-register', 'Auth\RegisterController@userRegister');
Route::post('/user-login', 'Auth\LoginController@login');
Route::post('/user-logout', 'Auth\LoginController@logout');

Route::post('users/{user}', 'WebsiteController@updateUser');
Route::post('questions', 'WebsiteController@storeQuestion');
Route::post('answers', 'WebsiteController@storeAnswer');
Route::post('comments', 'WebsiteController@storeComment');
Route::post('votes', 'WebsiteController@storeVote');
Route::get('get-categories', 'WebsiteController@getCategories');
Route::get('get-questions', 'WebsiteController@getQuestions');

//Route::get('/ask-question', 'WebsiteController@askQuestion');
//Route::get('/user-profile', 'WebsiteController@userProfile');
//Route::get('/user-questions', 'WebsiteController@userQuestions');
//Route::get('/user-answers', 'WebsiteController@userAnswers');
//Route::get('/user-points', 'WebsiteController@userPoints');

Auth::routes();

//Route::get('/home', 'HomeController@index')->name('home');
Route::get( '/{vue_route?}', 'WebsiteController@index' )->where( 'vue_route', '(.*)' );


