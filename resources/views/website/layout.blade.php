<!DOCTYPE html>
<!--[if IE 8 ]><html class="ie ie8" lang="en"> <![endif]-->
<!--[if IE 9 ]><html class="ie ie9" lang="en"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><!--><html lang="en"> <!--<![endif]-->
<head>
    <!-- Basic Page Needs -->
    <meta charset="utf-8">
    <title>@yield('title')</title>
    <meta name="description" content="Ask me Responsive Questions and Answers Template">
    <meta name="author" content="2code.info">
    <!-- Mobile Specific Metas -->
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <!-- Main Style -->
    <link rel="stylesheet" href="{{ asset('css/app.css') }}">
    <link rel="stylesheet" href="{{ asset('frontend/style.css') }}">
    <!-- Skins -->
    <link rel="stylesheet" href="{{ asset('frontend/css/skins/skins.css') }}">
    <!-- Responsive Style -->
    <link rel="stylesheet" href="{{ asset('frontend/css/responsive.css') }}">
    <link rel="stylesheet" href="{{ asset('taginpu/bootstrap-tagsinput.css') }}">
    <link rel="stylesheet" href="{{ asset('font-awesome/css/font-awesome.css') }}">
    <!-- Favicons -->
    <link rel="shortcut icon" href="{{ asset('frontend/images/favicon.png') }}">

    <script>
        @if(Auth::check())
            window.auth = {!! json_encode(Auth::user()); !!};
        @else
            window.auth = false;
        @endif
    </script>

</head>
<body>

<div class="loader"><div class="loader_html"></div></div>

<div id="wrap" class="grid_1200">
    <!-- Top Header -->
{{--    @include('website.included.top_header')--}}
    <!-- End Top Header -->

    <!--  Header -->
{{--    @include('website.included.header')--}}
    <!-- End header -->

    <!-- Main Content -->
    @yield('content')
    <!-- End Main Content -->

    <!-- Footer -->
{{--    @include('website.included.footer')--}}
    <!-- End Footer -->

</div><!-- End wrap -->
<div class="go-up"><i class="icon-chevron-up"></i></div>


<!-- js -->
<script src="{{ asset(('js/app.js')) }}"></script>
<script src="{{ asset(('frontend/js/jquery.min.js')) }}"></script>
<script src="{{ asset(('frontend/js/jquery-ui-1.10.3.custom.min.js')) }}"></script>
<script src="{{ asset(('frontend/js/jquery.easing.1.3.min.js')) }}"></script>
<script src="{{ asset(('frontend/js/html5.js')) }}"></script>
<script src="{{ asset(('frontend/js/twitter/jquery.tweet.js')) }}"></script>
<script src="{{ asset(('frontend/js/jflickrfeed.min.js')) }}"></script>
<script src="{{ asset(('frontend/js/jquery.inview.min.js')) }}"></script>
<script src="{{ asset(('frontend/js/jquery.tipsy.js')) }}"></script>
<script src="{{ asset(('frontend/js/tabs.js')) }}"></script>
<script src="{{ asset(('frontend/js/jquery.flexslider.js')) }}"></script>
<script src="{{ asset(('frontend/js/jquery.prettyPhoto.js')) }}"></script>
<script src="{{ asset(('frontend/js/jquery.carouFredSel-6.2.1-packed.js')) }}"></script>
<script src="{{ asset(('frontend/js/jquery.scrollTo.js')) }}"></script>
<script src="{{ asset(('frontend/js/jquery.nav.js')) }}"></script>
<script src="{{ asset(('frontend/js/tags.js')) }}"></script>
<script src="{{ asset(('frontend/js/jquery.bxslider.min.js')) }}"></script>
<script src="{{ asset(('frontend/js/custom.js')) }}"></script>
<script src="{{ asset(('taginpu/bootstrap-tagsinput.js')) }}"></script>
<!-- End js -->


</body>

</html>