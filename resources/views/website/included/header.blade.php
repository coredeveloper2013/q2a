<header id="header">
    <section class="container clearfix">
        <div class="logo"><a href="{{ url('/') }}"><img alt="" src="{{ asset('frontend/') }}/images/logo.png"></a></div>
        <nav class="navigation">
            <ul>
                <li class="{{  Request::is('/') ? 'current_page_item':'' }}">
                    <a href="{{ url('/') }}">Home</a>
                </li>

                <li class="ask_question {{  Request::is('ask-question') ? 'current_page_item':'' }} ">
                    <a href="{{ url('ask-question') }}">Ask Question</a>
                </li>
                <li><a href="cat_question.html">Questions</a>
                    <ul>
                        <li><a href="cat_question.html">Questions Category</a></li>
                        <li><a href="single_question.html">Question Single</a></li>
                        <li><a href="single_question_poll.html">Poll Question Single</a></li>
                    </ul>
                </li>
                <li class="{{  Request::is('user-*') ? 'current_page_item':'' }}">
                    <a href="#">User</a>
                    <ul>
                        <li class="{{  Request::is('user-profile') ? 'current_page_item':'' }}"><a href="{{ url('user-profile') }}">User Profile</a></li>
                        <li class="{{  Request::is('user-questions') ? 'current_page_item':'' }}"><a href="{{ url('user-questions') }}">User Questions</a></li>
                        <li class="{{  Request::is('user-answers') ? 'current_page_item':'' }}"><a href="{{ url('user-answers') }}">User Answers</a></li>
                        <li class="{{  Request::is('user-points') ? 'current_page_item':'' }}"><a href="{{ url('user-points') }}">User Points</a></li>
                        <li><a href="user_favorite_questions.html">User Favorite Questions</a></li>
                        <li><a href="edit_profile.html">Edit Profile</a></li>
                    </ul>
                </li>
                <li><a href="blog_1.html">Blog</a>
                    <ul>
                        <li><a href="blog_1.html">Blog 1</a>
                            <ul>
                                <li><a href="blog_1.html">Right sidebar</a></li>
                                <li><a href="blog_1_l_sidebar.html">Left sidebar</a></li>
                                <li><a href="blog_1_full_width.html">Full Width</a></li>
                            </ul>
                        </li>
                        <li><a href="blog_2.html">Blog 2</a>
                            <ul>
                                <li><a href="blog_2.html">Right sidebar</a></li>
                                <li><a href="blog_2_l_sidebar.html">Left sidebar</a></li>
                                <li><a href="blog_2_full_width.html">Full Width</a></li>
                            </ul>
                        </li>
                        <li><a href="single_post.html">Post Single</a>
                            <ul>
                                <li><a href="single_post.html">Right sidebar</a></li>
                                <li><a href="single_post_l_sidebar.html">Left sidebar</a></li>
                                <li><a href="single_post_full_width.html">Full Width</a></li>
                            </ul>
                        </li>
                    </ul>
                </li>
                <li><a href="right_sidebar.html">Pages</a>
                    <ul>
                        <li><a href="login.html">Login</a></li>
                        <li><a href="contact_us.html">Contact Us</a></li>
                        <li><a href="ask_question.html">Ask Question</a></li>
                        <li><a href="right_sidebar.html">Right Sidebar</a></li>
                        <li><a href="left_sidebar.html">Left Sidebar</a></li>
                        <li><a href="full_width.html">Full Width</a></li>
                        <li><a href="404.html">404</a></li>
                    </ul>
                </li>
                <li><a href="shortcodes.html">Shortcodes</a></li>
                <li><a href="contact_us.html">Contact Us</a></li>
            </ul>
        </nav>
    </section><!-- End container -->
</header>