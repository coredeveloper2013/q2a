import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

import index            from '../components/index'
import question         from '../components/question'
import askQuestion      from '../components/ask-question'
import userProfile      from '../components/user-profile'
import userPoints       from '../components/user-points'
import userAnswers      from '../components/user-answers'
import userQuestions    from '../components/user-questions'
import userEditProfile  from '../components/user-edit-profile'


export default new Router({
    mode: 'history',
    routes: [
        {path: '/',                  component: index, name: 'index'},
        {path: '/question',          component: question, name: 'question'},
        {path: '/ask-question',      component: askQuestion, name: 'askQuestion'},
        {path: '/user-profile',      component: userProfile, name: 'userProfile'},
        {path: '/user-points',       component: userPoints, name: 'userPoints'},
        {path: '/user-answers',      component: userAnswers, name: 'userAnswers'},
        {path: '/user-questions',    component: userQuestions, name: 'userQuestions'},
        {path: '/user-edit-profile', component: userEditProfile, name: 'userEditProfile'},
    ],
});
